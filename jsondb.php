<?php
/**
 * JSmart CMS
 * ===========================================================================
 * @author Vadim Shestakov
 * ---------------------------------------------------------------------------
 * @link https://jsmart.ru/
 * ---------------------------------------------------------------------------
 * @license https://jsmart.ru/cms/eula
 * ---------------------------------------------------------------------------
 * @copyright 2019 Vadim Shestakov
 * ===========================================================================
 */

class JSonDB
{
    var $file       = '';
    var $fileds     = [];
    var $elements   = [];
    var $comment    = '';
    var $insert_id  = '';

    /**
     * Инстализация
     *
     * @param string $database - путь до json файла
     * @param array $fileds - список допустимых полей
     * @param bool $create - создать файл если не он не существует
     */
    public function init($file, $fileds = [], $create = FALSE)
    {
        if ($create && !file_exists($file)) {
            file_put_contents($file, json_encode([]));
        }

        if (file_exists($file)) {
            $this->file = $file;
        }
        else {
            $this->error('DB file does not exist!');
        }

        if (count($fileds)) {
            $this->fileds = $fileds;
        }

        $json = json_decode(file_get_contents($this->file), TRUE);

        if (json_last_error() === JSON_ERROR_NONE && is_array($json))
        {
            if (isset($json['elements']) && count($json['elements'])) {
                $this->elements = $json['elements'];
            }

            if (!empty($json['comment'])) {
                $this->comment = $json['comment'];
            }
        }
    }

    /**
     * Запрос строк
     *
     * @param array $where - условия выборки
     * @return array - данные
     */
    public function query($where = [])
    {
        if (count($where))
        {
            $result = [];

            foreach ($this->where($where) as $key)
            {
                if (isset($this->elements[$key])) {
                    $result[] = $this->elements[$key];
                }
            }

            return $result;
        }

        return $this->elements;
    }

    /**
     * Запрос строки
     *
     * @param array $where - условия выборки
     * @return array данные
     */
    public function row($where)
    {
        foreach ($this->where($where) as $key)
        {
            if (isset($this->elements[$key])) {
                return $this->elements[$key];
            }
        }

        return FALSE;
    }

    /**
     * Запись строки
     *
     * @param array $data - данные для записи
     * @return boolean
     */
    public function insert($data = [])
    {
        $id     = uniqid();
        $insert = [];

        foreach ($this->fileds as $filed)
        {
            if (isset($data[$filed])) {
                $insert[$filed] = $data[$filed];
            }
            else {
                $insert[$filed] = '';
            }
        }

        if ($this->write(array_merge($this->elements, [array_merge(['id' => $id], $insert)]))) {
            $this->insert_id = $id;
            return TRUE;
        }

        return FALSE;
    }

    /**
     * ID последней записи
     *
     * @return string - ID
     */
    public function insert_id()
    {
        return $this->insert_id;
    }

    /**
     * Обновление строк
     *
     * @param array $data - данные для записи
     * @param array $where - условия выборки
     * @return boolean
     */
    public function update($data = [], $where = [])
    {
        $found = [];

        foreach ($this->where($where) as $key) {
            $found[$key] = $key;
        }

        if (count($found))
        {
            $update = [];

            foreach ($this->fileds as $filed)
            {
                if (isset($data[$filed])) {
                    $update[$filed] = $data[$filed];
                }
            }

            foreach ($found as $key) {
                if (isset($this->elements[$key])) {
                    $this->elements[$key] = array_merge($this->elements[$key], $update);
                }
            }

            return $this->write($this->elements);
        }

        return FALSE;
    }

    /**
     * Удаление строк
     *
     * @param array $where - условия выборки
     * @return boolean
     */
    public function delete($where = [])
    {
        $found = [];

        foreach ($this->where($where) as $key) {
            $found[$key] = $key;
        }

        if (count($found))
        {
            foreach ($found as $key) {
                if (isset($this->elements[$key])) {
                    unset($this->elements[$key]);
                }
            }

            return $this->write($this->elements);
        }

        return FALSE;
    }

    /**
     * Подсчет строк
     *
     * @param array $where - условия выборки
     * @return integer
     */
    public function count_all($where = [])
    {
        if (count($where)) {
            return count($this->where($where));
        }

        return count($this->elements);
    }

    /**
     * Максимальное значение поля
     *
     * @param string $field - поле
     * @param array $where - условия выборки
     * @return array - данные
     */
    public function select_max($field, $where = [])
    {
        return $this->select_max_min($field, $where,'max');
    }

    /**
     * Минимальное значение поля
     *
     * @param string $field - поле
     * @param array $where - условия выборки
     * @return array - данные
     */
    public function select_min($field, $where = [])
    {
        return $this->select_max_min($field, $where,'min');
    }

    /**
     * Выборка максимального и минимального значения поля
     *
     * @param string $field - поле
     * @param string $sort - тип выборки (max / min)
     * @param array $where - условия выборки
     * @return array
     */
    private function select_max_min($field, $where = [], $sort = 'max')
    {
        $found = [];

        if (count($where))
        {
            $found_where = [];

            foreach ($this->where($where) as $key) {
                $found_where[$key] = $key;
            }

            $data = [];

            foreach ($found_where as $key) {
                if (isset($this->elements[$key])) {
                    $data[$key] = $this->elements[$key];
                }
            }
        }
        else {
            $data =& $this->elements;
        }

        foreach ($data as $key => $value) {
            if (isset($value[$field])) {
                $found[$key] = $value[$field];
            }
        }

        if (count($found))
        {
            if ($sort == 'max') {
                arsort($found);
            }
            else {
                asort($found);
            }

            $key = array_key_first($found);

            if (isset($this->elements[$key])) {
                return $this->elements[$key];
            }
        }

        return FALSE;
    }

    /**
     * Выборка строк по условию
     *
     * @param array $where - условия выборки
     * @return array
     */
    private function where($where = [])
    {
        $found = [];

        if (count($where))
        {
            foreach ($this->elements as $key => $value)
            {
                $find = NULL;

                foreach ($where as $k => $v)
                {
                    if (isset($value[$k]) && $value[$k] == $v) {
                        if ($find !== FALSE) {
                            $find = TRUE;
                        }
                    }
                    else {
                        $find = FALSE;
                        break;
                    }
                }

                if ($find === TRUE) {
                    $found[$key] = $key;
                }
            }

            return $found;
        }
    }

    /**
     * Добавление комментария
     *
     * @param string $value
     * @return boolean
     */
    public function set_comment($value)
    {
        $this->comment = $value;

        return $this->write($this->elements);
    }

    /**
     * Запись в файл
     *
     * @param array $elements
     * @return boolean
     */
    private function write($elements = [])
    {
        $this->elements = $elements;

        return file_put_contents($this->file, json_encode([
            'comment'   => $this->comment,
            'elements'  => $elements
        ]));
    }

    /**
     * Вывод ошибки
     * @param string $message - сообщение ошибки
     */
    private function error($message = '')
    {
        exit($message);
    }
}